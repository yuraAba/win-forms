﻿using System.Windows.Forms;
using Services;
using System;
using Models;

namespace WindowsFormsApp
{
    public partial class Form1 : Form
    {
        private readonly DoctorService _doctorService;
        private readonly DoctorTypeService _doctorTypeService;
        private readonly AppointmentService _appointmentService;

        public Form1()
        {
            _doctorService = new DoctorService();
            _doctorTypeService = new DoctorTypeService();
            _appointmentService = new AppointmentService();

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listBox1.DataSource = _doctorTypeService.GetDoctorTypes();

            listBox1.DisplayMember = "Name";
            listBox1.ValueMember = "Id";
        }

        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            var lBox = (sender as ListBox) ?? new ListBox();
            var selectedItem = (lBox.SelectedItem as DoctorType) ?? new DoctorType();

            var doctors = _doctorService.GetDoctorsByType(selectedItem.Id);

            listBox2.DataSource = doctors;
            listBox2.DisplayMember = "Name";
            listBox2.ValueMember = "Id";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RandomDateTime date = new RandomDateTime();
            Appointment model = new Appointment
            {
                Id = 1,
                Date = date.Next(),
                DoctorName = listBox2.Text,
                PersonName = textBox1.Text,
                Problem = textBox4.Text,
            };

            bool isValid = true;

            if (string.IsNullOrEmpty(model.PersonName))
            {
                errorProvider1.SetError(textBox1, "Заповніть П.І.Б");
                isValid = false;
            }

            if (string.IsNullOrEmpty(model.Problem))
            {
                errorProvider2.SetError(textBox4, "Заповніть поле");
                isValid = false;
            }

            if (string.IsNullOrEmpty(model.DoctorName))
            {
                errorProvider3.SetError(listBox2, "Виберіть доктора");
                isValid = false;
            }

            if (isValid)
            {
                ClearFields();
                _appointmentService.SaveAppointment(model);
                MessageBox.Show($"Ви записані до {model.DoctorName} на {model.Date.ToString("MM/dd/yyyy hh:mm tt")}");
            }
        }

        private void ClearFields()
        {
            textBox1.Text = "";
            textBox4.Text = "";
        }

        class RandomDateTime
        {
            DateTime start;
            Random gen;
            int range;

            public RandomDateTime()
            {
                //start = new DateTime(1995, 1, 1);
                start = DateTime.Now;
                gen = new Random();
                range = (DateTime.Today - start).Days;
            }

            public DateTime Next()
            {
                return start.AddDays(gen.Next(range)).AddHours(gen.Next(0, 24)).AddMinutes(gen.Next(0, 60)).AddSeconds(gen.Next(0, 60));
            }
        }
    }
}
