﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services
{
    public class DoctorTypeService
    {
        private List<DoctorType> types;

        public DoctorTypeService()
        {
            types = new List<DoctorType>
            {
                new DoctorType
                {
                    Id = 1,
                    Name = "Type1"
                },
                new DoctorType
                {
                    Id = 2,
                    Name = "Type2"
                },
                new DoctorType
                {
                    Id = 3,
                    Name = "Type3"
                },
                new DoctorType
                {
                    Id = 4,
                    Name = "Type4"
                },
                new DoctorType
                {
                    Id = 5,
                    Name = "Type5"
                }
            };
        }

        public List<DoctorType> GetDoctorTypes()
        {
            return types;
        }
    }
}
