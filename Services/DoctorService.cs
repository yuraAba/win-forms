﻿using Models;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class DoctorService
    {
        private List<Doctor> doctors;

        public DoctorService()
        {
            doctors = new List<Doctor>
            {
                new Doctor
                {
                    Id = 1,
                    Name = "Doctor1",
                    TypeId = 1,
                },
                new Doctor
                {
                    Id = 2,
                    Name = "Doctor2",
                    TypeId = 5,
                },
                new Doctor
                {
                    Id = 3,
                    Name = "Doctor3",
                    TypeId = 3,
                },
                new Doctor
                {
                    Id = 4,
                    Name = "Doctor4",
                    TypeId = 1
                },
                new Doctor
                {
                    Id = 5,
                    Name = "Doctor5",
                    TypeId = 1
                },
                new Doctor
                {
                    Id = 6,
                    Name = "Doctor6",
                    TypeId = 2
                },
                new Doctor
                {
                    Id = 7,
                    Name = "Doctor7",
                    TypeId = 2
                },
                new Doctor
                {
                    Id = 8,
                    Name = "Doctor8",
                    TypeId = 1
                },
                new Doctor
                {
                    Id = 9,
                    Name = "Doctor9",
                    TypeId = 5
                },
                new Doctor
                {
                    Id = 10,
                    Name = "Doctor10",
                    TypeId = 1
                },
                new Doctor
                {
                    Id = 11,
                    Name = "Doctor11",
                    TypeId = 4
                },
                new Doctor
                {
                    Id = 12,
                    Name = "Doctor12",
                    TypeId = 3
                },
                new Doctor
                {
                    Id = 13,
                    Name = "Doctor13",
                    TypeId = 4
                },
            };
        }

        public List<Doctor> GetDoctors()
        {
            return doctors;
        }

        public Doctor GetDoctorById(int id)
        {
            return doctors.FirstOrDefault(d => d.Id == id);
        }

        public List<Doctor> GetDoctorsByType(int typeId)
        {
            return doctors.Where(d => d.TypeId == typeId).ToList();
        }
    }
}
