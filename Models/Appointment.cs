﻿using System;

namespace Models
{
    public class Appointment
    {
        public int Id { get; set; }
        public string PersonName { get; set; }
        public string DoctorName { get; set; }
        public DateTime Date { get; set; }
        public string Problem { get; set; }

        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}, {nameof(PersonName)}: {PersonName}, {nameof(DoctorName)}: {DoctorName}, {nameof(Date)}: {Date}, {nameof(Problem)}: {Problem}";
        }
    }
}
